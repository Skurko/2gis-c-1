﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lecture1_1
{
    class Program
    {
        static void Task1()
        {
            var a = Convert.ToDouble(Console.ReadLine());
            var b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("a + b = " + (a + b));
            Console.ReadKey();
        }

        static void Task2()
        {
            var inputNum = 1267165676175383;
            var temp = Convert.ToString(inputNum, 2);
            Console.WriteLine(temp);

            for (var i = (64 - temp.Length); i > 0; i--)
            {
                temp = '0' + temp;
            }

            Console.WriteLine(temp);

            var typeId = temp.Substring(0, 2);
            var cityId = temp.Substring(2, 15);
            var tableId = temp.Substring(17, 15);
            var objectId = temp.Substring(32);
            Console.WriteLine("binary type: " + typeId);
            Console.WriteLine("binary cityId: " + cityId);
            Console.WriteLine("binary tableId: " + tableId);
            Console.WriteLine("binary objectId: " + objectId);

            Console.WriteLine("typeId: " + Convert.ToInt32(typeId, 2));
            Console.WriteLine("cityId: " + Convert.ToInt32(cityId, 2));
            Console.WriteLine("tableId: " + Convert.ToInt32(tableId, 2));
            Console.WriteLine("objectId: " + Convert.ToInt32(objectId, 2));
            Console.ReadKey();
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Task #1");
            Task1();
            Console.WriteLine("-----------------------------\nTask #2");
            Task2();
        }
    }
}
